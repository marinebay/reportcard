<?php namespace ReportCard\TestCaseFactory;
use ReportCard\TestCase\TestCase as TestCase;

chdir(__DIR__);
require_once __DIR__.'/TestCaseAbstract.php';

class TestCaseFactory extends TestCase
{
    public $testCaseName;
    public $testCaseValue;
    public $testCaseVerification;
    public $testCaseResults;
    public $row = array();
    public $reporterArrayData = array();

    public function __construct(array $slug = array('testCaseName'=>'null','testCaseValue'=>'null',
        'testCaseVerification'=>'null', 'testCaseResults'=>'null'))
    {
        $this->setTestCaseName($slug['testCaseName']);
        $this->setTestCaseValue($slug['testCaseValue']);
        $this->setTestCaseVerification($slug['testCaseVerification']);
        $this->setTestCaseResults($slug['testCaseResults']);
    }
    
    public function setTestCaseName($name)
    {
         $this->testCaseName = $name;         
    }

    public function setTestCaseValue($value)
    {
         $this->testCaseValue = $value;         
    }

    public function setTestCaseVerification($verification)
    {
         $this->testCaseVerification = $verification;
    }
    
    public function setTestCaseResults($results)
    {
        $this->testCaseResults = $results;
    }

    public function setReporterArrayData(array $arrayData)
    {
        if(count($this->getReporterArrayData()))
        {
            $this->reporterArrayData[] = $arrayData;
        }
        else
        {
            $this->reporterArrayData = $arrayData;
        }
    }

    public function getTestCaseName($name)
    {
         return $this->testCaseName = $name;         
    }

    public function getTestCaseValue($value)
    {
         return $this->testCaseValue = $value;         
    }

    public function getTestCaseVerification($verification)
    {
         return $this->testCaseVarification = $verification;         
    }
    
    public function getTestCaseResults($results)
    {
        return $this->testCaseResults = $results;
    }

    public function setRow()
    {
        // Ensure we set all reporter row column values
        foreach ($this as $key => $value)
        {
            $this->row[$key] = (isset($value) && $key != 'row') ? $value : null;
        }
    }

    public function getRow()
    {
        return $this->row;
    }

    public function getReporterArrayData()
    {
        return $this->reporterArrayData;
    }

    public function pushReporterArrayData(array $array)
    {
        $reporterArrayData = $this->getReporterArrayData();

        if(empty($reporterArrayData))
        {
            $this->setReporterArrayData($array);
        }
        else
        {
            $this->reporterArrayData[] = $array;
        }
    }

    public function unsetReporterArrayData()
    {
        unset($this->reporterArrayData);
    }
}
