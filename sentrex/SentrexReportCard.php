<?php namespace ReportCard\SentrexGoLiveReportCard;
chdir(__DIR__);
require_once __DIR__.'/../ReportCardAbstract.php';
require_once __DIR__.'/../TestCaseFactoryClass.php';
require_once __DIR__.'/go_live_tests/TestMismatchedPersonsClass.php';

use ReportCard\ReportCard as ReportCard;
use ReportCard\TestMismatchedPersonsClass\TestMismatchedPersonsClass;

class SentrexGoLiveReportCard extends ReportCard
{
    public $contractId;
    public $ticketNumber;
    public $salesForceCollection;
    public $testCollection;

    public function __construct($slug)
    {
        $this->setTicketNumber($slug['ticketNumber']);
        $this->setSalesForceCollection($slug['salesForceUrl']);
    }

    public function setContractId($contractId)
    {
        $this->contractId = $contractId;
    }

    public function setTicketNumber($ticketNumber)
    {
        $this->ticketNumber = $ticketNumber;
    }

    public function setSalesForceCollection($salesForceCollection)
    {
        $this->salesForceCollection = $salesForceCollection;
    }

    public function setTestCollection($testCollection)
    {
        $this->testCollection = $testCollection;
    }

    public function getContractId()
    {
        return $this->contractId;
    }

    public function getTicketNumber()
    {
        return $this->ticketNumber;
    }

    public function getSalesForceCollection()
    {
        return $this->salesForceCollection;
    }

    public function getTestCollection()
    {
        return $this->testCollection;
    }

    public function reportCardReporterRowCollectionBag()
    {
        $testCaseFactory = new \ReportCard\TestCaseFactory\TestCaseFactory();
        echo "\n"; //['testCaseResults'];

        $testMismatchedPersonsClassTest = new \ReportCard\TestMismatchedPersonsClass\TestMismatchedPersonsClass();

        return $testMismatchedPersonsClassTest->createTestCaseReporterRow();
    }
}

$reportCard = array('ticketNumber'=>'1234567', 'salesForceUrl'=>'https://salesforce.com/?some_app_param');
$slug = array('ticketNumber'=>'1234567', 'salesForceUrl'=>'https://salesforce.com/?some_app_param');
$obj = new \ReportCard\SentrexGoLiveReportCard\SentrexGoLiveReportCard($slug);

print_r($obj->reportCardReporterRowCollectionBag());
