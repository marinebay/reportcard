<?php namespace ReportCard\TestMismatchedPersonsClass;

chdir(__DIR__);
require_once __DIR__.'/../../TestCaseFactoryClass.php';

use ReportCard\TestCaseFactory\TestCaseFactory;

class TestMismatchedPersonsClass
{
    public $percentage;
    public $acceptablePercentage;
    public $results = false;
    public $factory;

    public function setPercentage($getPercentage)
    {
        $this->percentage = $getPercentage;
    }

    public function getPercentage()
    {
        return $this->percentage;
    }

    public function setResults($results)
    {
        $this->results = $results;
    }

    public function getResults()
    {
        return $this->results;
    }

    public function setAcceptablePercentage($getAcceptablePercentage=.05)
    {
        $this->acceptablePercentage = $getAcceptablePercentage;
    }

    public function getAcceptablePercentage()
    {
        return $this->acceptablePercentage;
    }

    public function executeTestCase()
    {
        $this->setPercentage(.01);
        $this->setAcceptablePercentage(.02);
        $acceptablePercentage = $this->getAcceptablePercentage();
        $percentage = $this->getPercentage();

        if(isset($percentage) && isset($acceptablePercentage) && ($percentage <= $acceptablePercentage))
        {
            return 'true';
        }
        else
        {
           return 'false';
        }
    }

    public function createTestCaseReporterRow()
    {
        $this->setResults($this->executeTestCase());
        $slug = array('testCaseName' => 'Mismatched Persons Test', 'testCaseValue' => $this->getPercentage(),
            'testCaseVerification' => 'false', 'testCaseResults' => $this->getResults());

        $this->factory = new \ReportCard\TestCaseFactory\TestCaseFactory($slug);
        $this->factory->setRow();

        return $this->factory->getRow();
    }
}