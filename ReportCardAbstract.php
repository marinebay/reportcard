<?php namespace ReportCard;

/**
 * Class ReportCard
 * @package ReportCard
 * An abstract class to collect and bag critical data
 */
abstract class ReportCard
{
    public $ticketNumber;
    public $salesForceCollection;
    public $testCollection;

    abstract public function setTicketNumber($ticketNumber);
    abstract public function setSalesForceCollection($salesForceCollection);
    abstract public function setTestCollection($testCollection);
}
