<?php namespace ReportCard\TestCase;

abstract class TestCase 
{
    public $testCaseName;
    public $testCaseValue;
    public $testCaseVerification;
    public $testCaseResults;

    abstract public function setTestCaseName($name);
    abstract public function setTestCaseValue($value);
    abstract public function setTestCaseVerification($verification);
    abstract public function setTestCaseResults($resulst);
}
