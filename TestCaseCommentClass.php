<?php namespace TestCaseComment;

class TestCaseComment
{
    public $comment;

    public function setComment($comment=null)
    {
        $this->comment = $comment;
    }

    public function getComment()
    {
        return $this->comment;
    }
}